module "network-prod" {
  source = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-vpc-network/yandex-cloud/0.2.0"
  name   = "network-prod"
}

data "yandex_dns_zone" "dns-zone" {
  name = "todolist-public-zone"
}

module "dns-recordset-prod" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-recordset/yandex-cloud/0.2.0"
  depends_on = [module.vm-prod-balancer]
  zone_id    = data.yandex_dns_zone.dns-zone.id
  name       = "prod.va1erify.ru."
  type       = "A"
  ttl        = "200"
  data       = [module.vm-prod-balancer.external_ip]
}

module "dns-recordset-portainer" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-dns-recordset/yandex-cloud/0.2.0"
  depends_on = [module.vm-prod-balancer]
  zone_id    = data.yandex_dns_zone.dns-zone.id
  name       = "portainer.va1erify.ru."
  type       = "A"
  ttl        = "200"
  data       = [module.vm-prod-balancer.external_ip]
}

module "subnetwork-prod" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/vps-subnet/yandex-cloud/0.2.0"
  depends_on = [module.network-prod]
  id         = module.network-prod.id
  zone       = var.zone
  cidr_v4    = "192.168.1.0/24"
  name       = "subnetwork-prod"
}

module "vm-prod-balancer" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.1.0"
  depends_on           = [module.subnetwork-prod]
  subnetwork_id        = module.subnetwork-prod.id
  name                 = "prod01-balancer"
  internal_ip_address  = "192.168.1.3"
  hostname             = "prod01-balancer"
  platform             = "standard-v2"
  ram                  = "2"
  cpu                  = "2"
  core_fraction        = "100"
  boot_disk_image_id   = "fd8t849k1aoosejtcicj"
  boot_disk_size       = "5"
  boot_disk_type       = "network-hdd"
  boot_disk_block_size = "4096"
  nat                  = true
  preemptible          = false
  path_to_cloud_config = "./cloud-config"
}

module "vm-prod-db" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.1.0"
  depends_on           = [module.subnetwork-prod]
  subnetwork_id        = module.subnetwork-prod.id
  name                 = "prod01-db"
  internal_ip_address  = "192.168.1.4"
  hostname             = "prod01-db"
  platform             = "standard-v2"
  ram                  = "2"
  cpu                  = "2"
  core_fraction        = "100"
  boot_disk_image_id   = "fd8t849k1aoosejtcicj"
  boot_disk_size       = "5"
  boot_disk_type       = "network-hdd"
  boot_disk_block_size = "4096"
  nat                  = true
  preemptible          = false
  path_to_cloud_config = "./cloud-config"
}

module "vm-prod-app" {
  source               = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-compute-instance/yandex-cloud/0.1.0"
  depends_on           = [module.subnetwork-prod]
  subnetwork_id        = module.subnetwork-prod.id
  name                 = "prod01-app"
  internal_ip_address  = "192.168.1.5"
  hostname             = "prod01-app"
  platform             = "standard-v2"
  ram                  = "2"
  cpu                  = "2"
  core_fraction        = "100"
  boot_disk_image_id   = "fd8t849k1aoosejtcicj"
  boot_disk_size       = "5"
  boot_disk_type       = "network-hdd"
  boot_disk_block_size = "4096"
  nat                  = true
  preemptible          = false
  path_to_cloud_config = "./cloud-config"
}