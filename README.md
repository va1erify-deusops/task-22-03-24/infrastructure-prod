## Создаваемые сущности

- `module "network"`: Создание сети.
- `module "subnetwork"`: Создание подсети.
- `module "dns-recordset-prod"`: Создание dns записи для балансировщика.
- `module "dns-recordset-portainer"`: Создание dns для portainer.
- `module "vm-prod-balancer"`: Создание ВМ для балансировщика.
- `module "vm-prod-vm-prod-db"`: Создание ВМ для базы данных.
- `module "vm-prod-app"`: Создание ВМ для приложения.