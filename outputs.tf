output "vm_info" {
  value = {
    prod01-balancer = {
      name = module.vm-prod-balancer.hostname
      ip   = module.vm-prod-balancer.external_ip
    }
    prod01-db = {
      name = module.vm-prod-db.hostname
      ip   = module.vm-prod-db.external_ip
    }
    prod01-app = {
      name = module.vm-prod-app.hostname
      ip   = module.vm-prod-app.external_ip
    }
  }
}